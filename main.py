# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import argparse
import base64
import logging
import urllib.request as urllib
from time import sleep
from typing import List

from api.dtos.post_dto import PostDTO
from api.dtos.post_media_dto import PostMediaDTO
from api.post_api import get_posts_to_crawl, save_post_media
from constants.consts import RETRY_TIMEOUT, MAX_RETRY_TIMEOUT
from inscrawler.settings import override_settings
from inscrawler.settings import prepare_override_settings
from logger import start_logger


def usage():
    return """
        python main.py
        python main.py --debug
    """


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Instagram Crawler - Post downloader", usage=usage())

    prepare_override_settings(parser)

    args = parser.parse_args()

    override_settings(args)

    start_logger()

    retries = 1
    while True:
        try:
            logging.info("Getting list of posts to download...")
            posts_to_crawl: List[PostDTO] = get_posts_to_crawl()
            posts_urls = [("\'" + post.url + "\'") for post in posts_to_crawl]
            logging.info(
                f'List of awaiting(to be downloaded) posts returned. - Posts: {{{", ".join(posts_urls)}}}')
            for post in posts_to_crawl:
                logging.info(f"Downloading post medias of : \'{post.url}\'...")
                urls: List[str] = post.url_imgs

                for url_img in urls:
                    url_open_retries = 1
                    try:
                        u = urllib.urlopen(url_img)
                        url_open_retries = 1

                        meta = u.info()
                        extension = meta["Content-Type"]
                        content_length = meta["Content-Length"]

                        block_size = 8192
                        byte_array = []
                        while True:
                            buffer = u.read(block_size)
                            if not buffer:
                                break

                            byte_array.extend(buffer)

                        post_media: PostMediaDTO = PostMediaDTO(post_url=post.url, media_url=url_img,
                                                                extension=extension,
                                                                data=base64.b64encode(bytes(byte_array)).decode(
                                                                    'utf-8'))

                        while True:
                            try:
                                logging.info(f"Saving post media({url_img}) of post: \'{post.url}\'...")
                                save_post_media(post_media)
                                logging.info(f"Post media saved! - \'{url_img}\'")
                                break
                            except Exception as e:
                                logging.exception(e)
                                sleep(min(retries * RETRY_TIMEOUT, MAX_RETRY_TIMEOUT))
                                retries += 1

                    except Exception as e:
                        logging.exception(e)
                        sleep(min(url_open_retries * RETRY_TIMEOUT, MAX_RETRY_TIMEOUT))
                        url_open_retries += 1

        except Exception as e:
            logging.exception(e)
            sleep(min(retries * RETRY_TIMEOUT, MAX_RETRY_TIMEOUT))
            retries += 1

        sleep(5)
