import os

RETRY_TIMEOUT = int(os.environ.get('RETRY_TIMEOUT', 10))
MAX_RETRY_TIMEOUT = int(os.environ.get('MAX_RETRY_TIMEOUT', 60))
ENTITY_URL = os.environ.get('ENTITY_URL', '')
