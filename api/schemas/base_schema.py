from marshmallow import Schema, pre_load


class BaseSchema(Schema):
    SKIP_VALUES = {None}

    @pre_load
    def pre_load_filter(self, data, **kwargs):
        return {
            key: value for key, value in data.items()
            if value is not None
        }
