from typing import List

import requests

from api import post_schema
from api.dtos.post_dto import PostDTO
from api.dtos.post_media_dto import PostMediaDTO
from constants.consts import ENTITY_URL


def get_posts_to_crawl() -> List[PostDTO]:
    response = requests.get(f'{ENTITY_URL}/post/get_posts_to_crawl', params={'n_posts': 10})
    return post_schema.load(response.json(), many=True)


def save_post_media(post_media: PostMediaDTO):
    requests.post(f'{ENTITY_URL}/post_media', json=post_media.__dict__)
